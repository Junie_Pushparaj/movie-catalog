const BaseConfig = {
    searchService:{
        host: 'http://www.omdbapi.com/',
        i: 'tt3896198',
        apikey: 'fa281222'
    },
    noImageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/450px-No_image_available.svg.png',
    userName: 'Junie',
    moviesPerRow: 6,
    pagination: 10
}

export default BaseConfig;